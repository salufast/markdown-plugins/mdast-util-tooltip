import test from 'tape'
import {fromMarkdown} from 'mdast-util-from-markdown'
// Import {toMarkdown} from 'mdast-util-to-markdown'
import {tooltip} from 'micromark-extension-tooltip'
import {tooltipFromMarkdown} from './index.js'

test('markdown -> mdast', (t) => {
  t.deepEqual(
    fromMarkdown('[text]{def}', {
      extensions: [tooltip()],
      mdastExtensions: [tooltipFromMarkdown]
    }),
    {
      type: 'root',
      children: [
        {
          type: 'paragraph',
          children: [
            {
              type: 'html',
              value:
                '<span class="tooltip"><span class="tooltip-text">text</span><span class="tooltip-note">def</span></span>',
              position: {
                start: {line: 1, column: 1, offset: 0},
                end: {line: 1, column: 12, offset: 11}
              }
            }
          ],
          position: {
            start: {line: 1, column: 1, offset: 0},
            end: {line: 1, column: 12, offset: 11}
          }
        }
      ],
      position: {
        start: {line: 1, column: 1, offset: 0},
        end: {line: 1, column: 12, offset: 11}
      }
    }
  )
  t.end()
})
