/**
 * @typedef {import('mdast-util-from-markdown').Extension} FromMarkdownExtension
 * @typedef {import('mdast-util-from-markdown').Handle} FromMarkdownHandle
 * @typedef {import('mdast-util-to-markdown').Options} ToMarkdownExtension
 * @typedef {import('mdast-util-to-markdown').Handle} ToMarkdownHandle
 * @typedef {import('mdast').HTML} HTML
 */

/** @type {FromMarkdownExtension} */
export const tooltipFromMarkdown = {
  enter: {
    tooltipTextString: enterTooltipTextString,
    tooltipNoteString: enterTooltipNoteString
  },
  exit: {
    tooltip: exitTooltip,
    tooltipTextString: exitTooltipTextString,
    tooltipNoteString: exitTooltipNoteString
  }
}

// /** @type {ToMarkdownExtension} */

/** @type {FromMarkdownHandle} */
function enterTooltipTextString() {
  this.buffer()
}

/** @type {FromMarkdownHandle} */
function enterTooltipNoteString() {
  this.buffer()
}

/** @type {FromMarkdownHandle} */
function exitTooltip(token) {
  /** @type {string}
  /** @ts-expect-error It's fine */
  const text = this.getData('tooltipText')
  /** @type {string}
  /** @ts-expect-error It's fine */
  const note = this.getData('tooltipNote')
  this.enter({type: 'html', value: htmlValue(text, note)}, token)
  this.exit(token)
}

/**
 * @param {string} tooltipText
 * @param {string} tooltipNote
 * @returns {string}
 */
function htmlValue(tooltipText, tooltipNote) {
  const rawHtml =
    '<span class="tooltip">' +
    '<span class="tooltip-text">' +
    tooltipText +
    '</span>' +
    '<span class="tooltip-note">' +
    tooltipNote +
    '</span>' +
    '</span>'
  return rawHtml
}

/** @type {FromMarkdownHandle} */
function exitTooltipTextString() {
  const tooltipText = this.resume()
  this.setData('tooltipText', tooltipText)
}

/** @type {FromMarkdownHandle} */
function exitTooltipNoteString() {
  const tooltipNote = this.resume()
  this.setData('tooltipNote', tooltipNote)
}
